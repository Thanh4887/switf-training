//
//  ComplexUI.swift
//  SwitfUI
//
//  Created by SDC on 6/20/16.
//  Copyright © 2016 Dna.Cus. All rights reserved.
//

import UIKit

class ComplexUI: UIViewController, UIPickerViewDataSource , UIPickerViewDelegate {
    @IBOutlet weak var pkView: UIPickerView!
    
    var data = ["1","2","3"]

    override func viewDidLoad() {
        super.viewDidLoad()

        //pkView.delegate = self
        //pkView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        pkView.dataSource = self
        pkView.delegate = self
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int
    {
    return  1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return data.count;
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return data[row]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
