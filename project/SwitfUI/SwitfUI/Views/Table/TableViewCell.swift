//
//  TableViewCell.swift
//  SwitfUI
//
//  Created by SDC on 6/27/16.
//  Copyright © 2016 Dna.Cus. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var imgStatus: UIImageView!
    @IBOutlet weak var lblUsers: UILabel!
    @IBOutlet weak var lblTimeCalled: UILabel!
    @IBOutlet weak var btnCommand: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func statusImgSetting(typeCall: Int) {
        switch(typeCall){
            case 1: imgStatus.image = UIImage(named: "icBack")
            case 2: imgStatus.image = UIImage(named: "icBack")
            case 3: imgStatus.image = UIImage(named: "icBack")
            break
        }
    }
    
    func usersSetting(user: String, numCall: Int) {
        self.lblUsers.text = user + "(\(numCall))"
    }
    
    func timeCallSetting(timeCall: NSDate) {
        let mydateFormatter = NSDateFormatter()
        mydateFormatter.dateFormat = "dd/MM/yyyy'T'HH:mm:ss+00:00"
        mydateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
        self.lblTimeCalled.text = mydateFormatter.stringFromDate(timeCall)
    }
    
}
