//
//  ViewTableController.swift
//  SwitfUI
//
//  Created by SDC on 6/27/16.
//  Copyright © 2016 Dna.Cus. All rights reserved.
//

import UIKit

class ViewTableController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tblView: UITableView!
    
    var listItem = ["abc","def","ghi","jkl","mno","pqr", "stx"]
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.dataSource = self
        tblView.delegate = self
        
        let cellIdentifier = "TableViewCell"
        let cell = tblView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: NSIndexPath) as! TableViewCell
        
        let table = UINib(nibName: "TableViewCell", bundle:nil)
        tblView.registerNib(table, forCellReuseIdentifier: "TableViewCell")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if (section == 0)
        return listItem.filter({ (String) -> Bool in <#code#> })
        
        return listItem.count / 2
    }
    
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell =  UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: <#T##String?#>)
        
        cell.textLabel?.text = "Section \(indexPath.section) Row\(indexPath.row)"
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        return
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
